terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">=17"
    }
  }
}

resource "gitlab_group" "groups" {
  for_each = var.groups

  name                              = each.value.name
  path                              = each.value.path
  parent_id                         = var.parent_id
  auto_devops_enabled               = each.value.auto_devops_enabled
  avatar                            = each.value.avatar
  avatar_hash                       = try(filesha256(each.value.avatar), null)
  default_branch_protection         = each.value.default_branch_protection
  description                       = each.value.description
  lfs_enabled                       = each.value.lfs_enabled
  mentions_disabled                 = each.value.mentions_disabled
  prevent_forking_outside_group     = each.value.prevent_forking_outside_group
  project_creation_level            = each.value.project_creation_level
  request_access_enabled            = each.value.request_access_enabled
  require_two_factor_authentication = each.value.require_two_factor_authentication
  share_with_group_lock             = each.value.share_with_group_lock
  subgroup_creation_level           = each.value.subgroup_creation_level
  two_factor_grace_period           = each.value.two_factor_grace_period
  visibility_level                  = each.value.visibility_level
}
