variable "groups" {
  type = map(object({
    name                              = string
    path                              = string
    auto_devops_enabled               = optional(bool, false)
    avatar                            = optional(string, null)
    default_branch_protection         = optional(number, 2)
    description                       = optional(string, "")
    emails_disabled                   = optional(bool, false)
    lfs_enabled                       = optional(bool, true)
    mentions_disabled                 = optional(bool, false)
    prevent_forking_outside_group     = optional(bool, false)
    project_creation_level            = optional(string, "maintainer")
    request_access_enabled            = optional(bool, false)
    require_two_factor_authentication = optional(bool, false)
    share_with_group_lock             = optional(bool, false)
    subgroup_creation_level           = optional(string, "maintainer")
    two_factor_grace_period           = optional(number, 48)
    visibility_level                  = optional(string, "private")
    })
  )
}

variable "parent_id" {
  description = "Parent group id"
  type        = number
  default     = 0
}
